# -*- coding: utf-8 -*-
"""
Created on Tue Nov  3 11:28:26 2020

@author: Kalen
"""

import numpy as np
from scipy.optimize import curve_fit
from scipy.integrate import odeint
import matplotlib.pyplot as plt

# This solution was created using Dr. Ford Versypts reference material & code

# Data for example 2
xaxisData = np.array([0.01667, 0.03333, 0.05, 0.1])  # time, independent variable
# new for > 1 dependent variables: for multiple rows, put each row in a [] and surround the whole thing by ([])
yaxisData = np.array([[0.5074, 0.3796, 0.2882, 0.1762], [0.3767, 0.4385, 0.4865, 0.5416],
                      [0.0885, 0.1360, 0.1618, 0.2108], [0.0274, 0.0459, 0.0572, 0.0714]])  # x, dependent variable - VGO, Gasoline, Gas, Coke

# guesses for parameters
k12guess = 1.0
k13guess = 1.0
k14guess = 1.0
k23guess = 1.0
k24guess = 1.0

parameterguesses = np.array([k12guess, k13guess, k14guess, k23guess, k24guess])


# Need two functions for our model
# 1. to define the system of ODE(s)
# 2. to solve the ODE(s) and return ypredicted values in same shape as yaxisData

# 1. define ODEs
def system_of_ODEs(y, t, parameters):  # yvar, xvar, args

    # unpack the parameters
    k12 = parameters[0]
    k13 = parameters[1]
    k14 = parameters[2]
    k23 = parameters[3]
    k24 = parameters[4]
    
    # unpack the dependent variables
    y1 = y[0]
    y2 = y[1]
    y3 = y[2]
    y4 = y[3]
    
    dy1dt = -(k12 + k13 + k14) * y1 ** 2
    dy2dt = k12 * y1 ** 2 - k23 * y2 - k24 * y2
    dy3dt = k13 * y1 ** 2 + k23 * y2
    dy4dt = k14 * y1 ** 2 + k24 * y2

    return dy1dt, dy2dt, dy3dt, dy4dt

# 2. Solve ODEs at xaxisData points
# and return calculated yaxisCalculated
# using current values of the parameters
def model(xaxisData, *params):
    # initial condition(s) for the ODE(s)
    yaxis0 = np.array([1.0, 0.0, 0.0, 0.0])  # should include a decimal
    # new for > 1 dependent variables:
    numYaxisVariables = 4
    yaxisCalc = np.zeros((xaxisData.size, numYaxisVariables))

    for i in np.arange(0, len(xaxisData)):
        if xaxisData[i] == 0.0:  # should include a decimal
            # edit for > 1 dependent variables:
            yaxisCalc[i, :] = yaxis0
        else:
            xaxisSpan = np.linspace(0.0, xaxisData[i], 101)
            ySoln = odeint(system_of_ODEs, yaxis0, xaxisSpan, args=(params,))  # soln for entire xaxisSpan
            # edit for > 1 dependent variables:
            yaxisCalc[i, :] = ySoln[-1, :]  # calculated y at the end of the xaxisSpan
            # at this point yaxisCalc is now 2D matrix with the number of columns set as : to include all yvariables
            # curve_fit needs a 1D vector that has the rows in a certain order, which result from the next two commands
    yaxisOutput = np.transpose(yaxisCalc)
    yaxisOutput = np.ravel(yaxisOutput)
    return yaxisOutput

# Estimate the parameters
# new for > 1 dependent variables:
# np.ravel(yaxisData) transforms yaxisData from a 2D vector into the 1D vector that curve_fit expects.

parametersoln, pcov = curve_fit(model, xaxisData, np.ravel(yaxisData), p0=parameterguesses)
print('The value for k12 is:',parametersoln[0])
print('The value for k13 is:',parametersoln[1])
print('The value for k14 is:',parametersoln[2])
print('The value for k23 is:',parametersoln[3])
print('The value for k24 is:',parametersoln[4])
# print('The value for k1 is:{parametersoln[0]:.2f}'.format(parametersoln[0]=parametersoln[0]))
# edit for > 1 dependent variables:
xdataConversion = 1 - yaxisData[0, :]
plt.plot(xdataConversion, yaxisData[1, :], 'bs')
plt.plot(xdataConversion, yaxisData[2, :], 'yx')
plt.plot(xdataConversion, yaxisData[3, :], 'ro')
plt.legend(['Gasoline', 'Gas','Coke'])
# initial condition(s) for the ODE(s)
yaxis0 = np.array([1.0, 0.0, 0.0,0.0])  # should include a decimal
numYaxisVariables = 4

xaxisForPlotting = np.linspace(0, xaxisData[-1], 101)

# Two options for getting the solution:
# OptionA call the model, which returns a 1D output and reshape into 2D
# OptionB wrap odeint around system_of_ODEs to solve the differential equations directly

# OptionA
# yaxisCalc_OptionA = model(xaxisForPlotting, *parametersoln)
# the answer from model is 1D so we need to reshape it into the expected 2D matrix dimensions for plotting
# yaxisCalc_OptionA = np.reshape(yaxisCalc_OptionA, (numYaxisVariables, xaxisForPlotting.size))
# plt.plot(xaxisForPlotting, yaxisCalc_OptionA[0, :], 'b-', label='y1 fitted')
# plt.plot(xaxisForPlotting, yaxisCalc_OptionA[1, :], 'r-', label='y2 fitted')
# plt.plot(xaxisForPlotting, yaxisCalc_OptionA[2, :], 'o-', label='y3 fitted')

## OptionB

yaxisCalc_OptionB = odeint(system_of_ODEs, yaxis0, xaxisForPlotting, args=(parametersoln,))
Conversion = 1 - yaxisCalc_OptionB[:, 0]
plt.plot(Conversion, yaxisCalc_OptionB[:, 1], 'b-')
plt.plot(Conversion, yaxisCalc_OptionB[:, 2], 'y-')
plt.plot(Conversion, yaxisCalc_OptionB[:, 3], 'r-')
# From the plot we see that OptionA and OptionB give exactly the same result, so you can chose either and not have to use both options.

# yaxisCalcFromGuesses = odeint(system_of_ODEs, yaxis0, xaxisForPlotting, args=(parameterguesses,))
# plt.plot(xaxisForPlotting, yaxisCalcFromGuesses, 'k-')  # before fitting
plt.xlabel('conversion')
plt.ylabel('yield')
plt.show()

# this plots yi verses time
plt.plot(xaxisData, yaxisData[1, :], 'bs')
plt.plot(xaxisData, yaxisData[2, :], 'yx')
plt.plot(xaxisData, yaxisData[3, :], 'ro')
plt.plot(xaxisForPlotting, yaxisCalc_OptionB[:, 1], 'b-')
plt.plot(xaxisForPlotting, yaxisCalc_OptionB[:, 2], 'y-')
plt.plot(xaxisForPlotting, yaxisCalc_OptionB[:, 3], 'r-')
plt.legend(['Gasoline', 'Gas','Coke'])
plt.xlabel('time')
plt.ylabel('yield')

plt.show()
