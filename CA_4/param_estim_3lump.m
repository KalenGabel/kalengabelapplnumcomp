%% Computational Assignment 04
% *Author:* Kalen S. Gabel
%
% *Instructor:* Dr. Ford Versypts
%
% *Course:* CHE 5753 - Applied Numerical Computing for Scientists and
% Engineers
%
% *Assignment:* Computational Assigment 04
% 
% *Due Date:* 10/21/20
% 
%
%% Function Description
%
% This function was written by Kalen S. Gabel on October 30th, 2020.
%
% The function performs parameter estimation on a kinetic model for an industrial process. 
% The model simulates a fluid catalytic cracker that converts petroleum
% refining intermediates into more vaulable lighter products. The most
% common models are the three lump and four lump model. This codes uses the
% three lump model.

%% Function that performs parameter estimation on a systems of ODEs

function param_estim_3lump()
clf
clear all

tdata = [0.01667 0.0333 0.0500 0.1000]; % independent variable, x-axis
y1data = [0.5074 0.3796 0.2882 0.1762]; % dependent variables, y-axis - VGO
y2data = [0.3767 0.4385 0.4865 0.5416]; % dependent variables, y-axis - Gasoline
y3data = [0.1159 0.1819 0.2253 0.2822]; % dependent variables, y-axis - Gas + Coke

ydata = [y1data;y2data;y3data];

conversion = 1.- y1data;

y0 = [1 0 0]; % initial conditions [ x1(0) x2(0)];

% Parameter guesses
k1guess = 1;
k2guess = 1;
k3guess = 1;
parameterguesses = [k1guess, k2guess, k3guess];

% Estimate parameters k1 & k2 & k3

parameters = lsqcurvefit(@(parameterguesses,tdata)model(parameterguesses,tdata,y0),parameterguesses, tdata, ydata);

% parameters here are for printing to the output screen
k1 = parameters(1)
k2 = parameters(2)
k3 = parameters(3)

% this section compiles data & model results to be plotted 
tforplotting = linspace(tdata(1),tdata(end),101);
timespan = linspace(0,1,101);
xatguesses = model(parameterguesses, tforplotting,y0);
xatsoln = model(parameters,tforplotting,y0);
xatsoln1 = model(parameters,tdata,y0);
xconversion = 1 - xatsoln(1,:);


% Plots
% first figure uses conversion for x-axis
figure(1)
plot(xconversion,xatsoln(1,:),'b',xconversion,xatsoln(2,:),'r',xconversion,xatsoln(3,:),'g')
hold on 
plot(conversion,ydata(1,:),'bs',conversion,ydata(2,:),'rx',conversion,ydata(3,:),'go')
legend('VGO predicted','Gasoline predicted', 'Gas+Coke predicted','VGO','Gasoline', 'Gas+Coke')
xlim([0,1])
ylim([0,1])
xlabel('Conversion, wt fraction')
ylabel('Yield, wt fraction')
hold off
% second figure uses time for x-axis
figure(2)
plot(tdata,xatsoln1(1,:),'b',tdata,xatsoln1(2,:),'r',tdata,xatsoln1(3,:),'g')
hold on 
plot(tdata,ydata(1,:),'bs',tdata,ydata(2,:),'rx',tdata,ydata(3,:),'go')
legend('VGO predicted','Gasoline predicted', 'Gas+Coke predicted','VGO','Gasoline', 'Gas+Coke')
xlim([0,0.2])
ylim([0,1])
xlabel('Time, hours')
ylabel('Yield, wt fraction')
hold off

% function created from class code and Dr. Ford Versypts reference material
function output = model(parameters,t,y0)
    for i = 1:length(t)
        if t(i) == 0 
            tsoln = 0;
            ysoln = y0;
            output(:,i) = ysoln;
        else
            tspan = [0 t(i)]; 
            [tsoln, ysoln] = ode23s(@(t,y) system_of_ODEs(t,y,parameters), tspan, y0);
            output(i,:) = ysoln(end,:);
        end
    end
    output = output';
end

% this fuction defines our system of ODEs using the three lump model
function output = system_of_ODEs(t,y,parameters)
    
    % Three lump model
    k1 = parameters(1);
    k2 = parameters(2);
    k3 = parameters(3);
    
    y1 = y(1);
    y2 = y(2);
    y3 = y(3);
    
    dydt(1) = -(k1+k3)*y1^2;
    dydt(2) = k1*y1^2 - k2*y2;
    dydt(3) = k3*y1^2 + k2*y2;
    
    output = dydt';
end

end