% Reading in data
T = xlsread('Compressor_Data.xlsx');
Tcond_range = T(:,1); % degreeF
Tevap_range = T(:,2); % degreeF
MassFlow_Data = T(:,3); % lb/hr
Workinput_Data = T(:,4); % W

Tevap = 20; % F
Tcond = 100; % F

Temps = [Tevap_range, Tcond_range];

% Setting initial guesses for the parameter guesses in lsqcurvefit
guesses = [1,1,1,1,1,1,1,1,1,1];
% Setting a varibale named mass flow rate data to pass
mdotdata = MassFlow_Data;
wdotdata = Workinput_Data;
% Setting a function to be fitted to. Parameters are: 'a'
mdot = @(a,Temps) a(1) + a(2).*Temps(:,1) + a(3).*Temps(:,2) + a(4).*Temps(:,1).^2 + a(5).*Temps(:,1).*Temps(:,2) + a(6).*Temps(:,2).^2 + a(7).*Temps(:,1).^3 + a(8).*Temps(:,2).*Temps(:,1).^2 + a(9).*Temps(:,1).*Temps(:,2).^2 + a(10).*Temps(:,2).^3;
Wdot = @(b,Temps) b(1) + b(2).*Temps(:,1) + b(3).*Temps(:,2) + b(4).*Temps(:,1).^2 + b(5).*Temps(:,1).*Temps(:,2) + b(6).*Temps(:,2).^2 + b(7).*Temps(:,1).^3 + b(8).*Temps(:,2).*Temps(:,1).^2 + b(9).*Temps(:,1).*Temps(:,2).^2 + b(10).*Temps(:,2).^3;
% Solving for the parameters in mass flow rate 
mdotcoefs = lsqcurvefit(mdot, guesses,Temps,mdotdata);
% Solving for the parameters in power equation
powercoefs = lsqcurvefit(Wdot, guesses,Temps,wdotdata);

% =========================================================================

% Testing the parameters by passing them to the below functions
temps = [Tevap,Tcond];
mdot_test = AHRI_massflow(mdotcoefs,temps)
power_test = AHRI_massflow(powercoefs,temps) % passing the power coefs in
% To verfify with 100F cond and 20F evap i went to the excel data sheet.
mdot_verify = T(4,3)
power_verify = T(4,4)


% =============================================================== Ignore == 
% z (mdot) needs to be mxn matrix
% [X,Y] = meshgrid(Tevap_plotting,Tcond_plotting);
% c = mdotcoefs;
% mdot1 = c(1) + c(2).*X + c(3).*Y + c(4).*X.^2 + c(5).*X.*Y + c(6).*Y.^2 + c(7).*X.^3 + c(8).*Y.*X.^2 + c(9).*X.*Y.^2 + c(10).*Y.^3;
% surf(mdot1,X,Y)
% =========================================================================
function massflow = AHRI_massflow(parameters,tempdata)
    
    Te = tempdata(1);
    Tc = tempdata(2);
    
    a1 = parameters(1);
    a2 = parameters(2);
    a3 = parameters(3);
    a4 = parameters(4);
    a5 = parameters(5);
    a6 = parameters(6);
    a7 = parameters(7);
    a8 = parameters(8);
    a9 = parameters(9);
    a10 = parameters(10);
    
    mdot = a1 + a2*Te + a3*Tc + a4*Te.^ 2 + a5*Te*Tc + a6*Tc.^2 + a7*Te.^3 + a8*Tc*Te.^2 + a9*Te*Tc.^2 + a10*Tc.^3;
    
    massflow = mdot;
    %Wdot = b1 + b2 * Ts + b3 * Td + b4 * Ts ** 2 + b5 * Ts * Td + b6 * Td ** 2 + b7 * Ts ** 3 + b8 * Td * Ts ** 2 + b9 * Ts * Td ** 2 + b10 * Td ** 3
    
end