%% Computational Assignment 02
% *Author:* Kalen S. Gabel
%
% *Instructor:* Dr. Ford Versypts
%
% *Course:* CHE 5753 - Applied Numerical Computing for Scientists and
% Engineers
%
% *Assignment:* Computational Assigment 02
% 
% *Due Date:* 09/15/20
% 


%% Function Description
%
% This function was written by Kalen S. Gabel on Septemeber 12th, 2020.
%
% The function can take in a varible number of inputs, which are; concentrations and 
%   rate constants, and return rate of change of the mass concentration of 
%   species A & B at a certain hour, t.
%
% Below are the equations that this function uses to ouput its
% concentration rates.
% 
% $$\frac{dC_a}{dt} = -k_{1}C_{a} - k_{2}C_{a} $$
% 
% $$\frac{dC_b}{dt} = k_{1}C_{a} - k_{3} - k_{4}C_{b} $$
% 
% Where $$\frac{dC_i}{dt} $$ is the change in concentraion of a certain
% species at time $$ t $$ and the $$ C_{i} $$ terms are the mass
% concetration of a species, and the $$ k_{i} $$ terms are the rate constants for this particular reaction.
%
% The default values are as follows: 
%
% * t = 0;        hrs 
% * Ca = 6.25;    mg / L
% * Cb = 0;       mg / L
% * k1 = 0.15;    hrs^-1
% * k2 = 0.6;     hrs^-1
% * k3 = 0.1;     mg / L * hrs
% * k4 = 0.15;    hrs^-1


%% Testing the created function
%
% This section is added to test the function during development and for
%   grading.
%
% Ca = 4; % setting a value for the concentration of A to pass into the C row vector
% Cb = 1; % setting a value for the concentration of B to pass into the C row vector 
% C = [Ca,Cb]; % setting a column vector to pass in as C where Ca and Cb are the first and second entries in the array, respectively. 
% x = system_of_ODEs() % variable named to take the outputs of the function

%% Mass Concentration Function 
%
% This section contains the funtion
%
function output = system_of_ODEs(varargin)

    if nargin < 1 % if we have less than 1 arguement, the default value for 't' is zero
        t = 0; % hrs
    else
        t = varargin{1}; % if there is greater than one input, the first input will be taken as time 't'
    end
    if nargin < 2 % if there is less than 2 inputs the default values for Ca and Cb are used  
        Ca = 6.25; % mg / L
        Cb = 0; % mg / L
    else
        C = varargin{2}; % if there is greater than 2 input, the second entry will be used as the input C vector 
        Ca = C(1); % mg / L
        Cb = C(2); % mg / L
    end
    if nargin < 3 % if there is less than 3 inputs, we set k1 equal to its default
        k1 = 0.15; % hrs^-1
    else
        k1 = varargin{3}; % if there is greater than 3 inputs, the third entry will be used as the input for k1
    end
    if nargin < 4 % if there is less than 4 inputs, the default value for k2 will be used
        k2 = 0.6;  % hrs^-1
    else
        k2 = varargin{4}; % if there is greater than 4 inputs, the fourth entry will be used as the input for k2
    end
    if nargin < 5 % if there is less than 5 inputs, the default value for k3 will be used
        k3 = 0.1; % mg / L * hrs
    else
        k3 = varargin{5}; % if there is greater than 5 inputs, the fifth entry will be used as the input for k3
    end
    if nargin < 6 % if there is less than 6 inputs, the default value for k4 will be used
        k4 = 0.2; % hrs^-1
    else
        k4 = varargin{6}; % if there is greater than 6 inputs, the sixth entry will be used as the input for k2
    end
    
    % The if statement below handles the case of passing too many inputs to the function
    % In the event that more than 6 values are passed to the
    % function, the function will evaluate variables as their default values.
    
    if nargin > 6 
        
        t = 0;          % hrs
        Ca = 6.25;      % mg / L
        Cb = 0;         % mg / L
        k1 = 0.15;      % hrs^-1
        k2 = 0.6;       % hrs^-1
        k3 = 0.1;       % mg / L * hrs
        k4 = 0.2;      % hrs^-1
        
    end
    
    
    % Defining the given derivitave equations to pass values to
    dCadt = (-k1*Ca - k2*Ca);
    dCbdt = (k1*Ca - k3 - k4*Cb);
    
    % defining a 2x1 column vector for the function to return
    % it is returning the mass concentration of species A & B at hour t
    output = [dCadt;dCbdt];
    
end


