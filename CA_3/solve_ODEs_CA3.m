%% Computational Assignment 02
% *Author:* Kalen S. Gabel
%
% *Instructor:* Dr. Ford Versypts
%
% *Course:* CHE 5753 - Applied Numerical Computing for Scientists and
% Engineers
%
% *Assignment:* Computational Assigment 03
% 
% *Due Date:* 10/02/20
% 
%
%% Function Description
%
% This function was written by Kalen S. Gabel on Septemeber 30th, 2020.
%
% The function solves a system of ODEs describing the molar flow rates and
% temperature as volume changes through a plug flow reactor. 

%% Function that solves systems of ODE's


function solve_ODEs_CA3
clear variables;
clf;
clc;

% defining parameter values
Vspan = [0 1];
T = 423;
Fa = 100;
Fb = 0;
Fc = 0;

%Constants
T0 = 423; % K
Ct0 = 0.1; % J/mol
Cpa = 90; % J/mol
Cpb = 90; % J/mol
Cpc = 180; % J/mol
Ua = 4000; % J/m^3-s-C
Ta = 373; % C (constant)
E1_over_R = 4000; % Kelvin
E2_over_R = 9000; % Kelvin
Hr1a = -20000; % J/(mol of A reacted in reaction 1)
Hr2a = -60000; % J/(mol of A reacted in reaction 2)

% inital temperature and molar flow rates
ic = [T, Fa, Fb, Fc];
% plotting the results 
[t,X] = ode45(@(V,X) ODEs_CA3(V,X),Vspan,ic);
figure(1)
plot(t,X(:,1),'blue');
title('Temperature vs Volume');
xlabel('Volume [m^3]');
ylabel('Temperature [?C?]');
legend('Temperature');
pause

figure(2)
plot(t,X(:,2),'blue',t,X(:,3),'red',t,X(:,4),'green');
title('xxx');
xlabel('Volume [dm^3]');
ylabel('Molar Flow Rates [mol/s]');
legend('Fa','Fb','Fc');
pause

function outputs = ODEs_CA3(V,X)
    
    T = X(1);
    Fa = X(2);
    Fb = X(3);
    Fc = X(4);
        
    Ft = Fa + Fb + Fc;
    Ca = Ct0*(Fa/Ft)*(T0/T);
    Cb = Ct0*(Fb/Ft)*(T0/T);
    Cc = Ct0*(Fc/Ft)*(T0/T);
    
    k1a = 10*exp(E1_over_R*((1/300)-(1/T))); % s^-1, temperature in K
    k2a = 0.09*exp(E2_over_R*((1/300)-(1/T))); % dm^3/mol-s
    
    r1a = -k1a*Ca;
    r2a = -k2a*(Ca^2);
    
    % Net Rates 
    ra = r1a+r2a;
    rb = k1a*Ca;
    rc = 0.5*k2a*(Ca^2);
    
    % Defining mole balances    
    dFadV = ra; % molar flow rate [mol/s]
    dFbdV = rb; % molar flow rate [mol/s]
    dFcdV = rc; % molar flow rate [mol/s]
    dTdV = (Ua*(Ta-T)+(-r1a*-Hr1a)+(-r2a*-Hr2a))/(Fa*Cpa+Fb*Cpb+Fc*Cpc);
    
    outputs = [dTdV;dFadV;dFbdV;dFcdV];

end


end


