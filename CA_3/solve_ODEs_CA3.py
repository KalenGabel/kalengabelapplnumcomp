# -*- coding: utf-8 -*-
"""
Created on Fri Sep 25 16:07:02 2020

@author: Kalen
"""
# Computational Assignment 03
# Author: Kalen S. Gabel
# Instructor: Dr. Ford Versypts
# Course: CHE 5753 - Applied Numerical Computing for Scientists and Engineers
# Assignment: Computational Assigment 03
# Due Date: 10/02/20

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint


def ODEs_CA3(initial_conditions,V,parameters):
    #parameters = (T0,E1_over_R,E2_over_R,Ct0,Cpa,Cpb,Cpc,Ta,Hr1a,Hr2a)
    # constants passed in through the parameters list
    T0 = parameters[0]
    E1_over_R = parameters[1]
    E2_over_R = parameters[2]
    Ct0 = parameters[3]
    Cpa = parameters[4]
    Cpb = parameters[5]
    Cpc = parameters[6]
    Ta = parameters[7]
    Hr1a = parameters[8]
    Hr2a = parameters[9]
    Ua = parameters[10]
    
    [T,Fa,Fb,Fc] = initial_conditions
    
    # -------------------------
    # Stoichoimetry 
    Ft = Fa + Fb + Fc
    Ca = Ct0*(Fa/Ft)*(T0/T)
    Cb = Ct0*(Fb/Ft)*(T0/T)
    Cc = Ct0*(Fc/Ft)*(T0/T)
    
    k1a = 10*np.exp(E1_over_R*((1/300)-(1/T))) # s^-1, temperature in K
    k2a = 0.09*np.exp(E2_over_R*((1/300)-(1/T))) # dm^3/mol-s
    
    r1a = -k1a*Ca
    r2a = -k2a*(Ca**2)

    # Net Rates 
    ra = r1a+r2a
    rb = k1a*Ca
    rc = 0.5*k2a*(Ca**2)

    # Defining mole balances
    dFadV = ra # molar flow rate [mol/s]
    dFbdV = rb # molar flow rate [mol/s]
    dFcdV = rc # molar flow rate [mol/s]
    
    dTdV = (Ua*(Ta-T)+(-r1a*-Hr1a)+(-r2a*-Hr2a))/(Fa*Cpa+Fb*Cpb+Fc*Cpc) # temperature [K]
    
    return [dTdV,dFadV,dFbdV,dFcdV]

#-----------------------------------------------------------------------------
def main():
    # defining parameter values
    Vspan = np.linspace(0,1,101)
    V1 = 0
    T = 423
    Fa = 100
    Fb = 0
    Fc = 0
    
    # Constants
    T0 = 423
    Ct0 = 0.1 
    Cpa = 90 # J/mol
    Cpb = 90 # J/mol
    Cpc = 180 # J/mol
    Ua = 4000 # J/m^3-s-C
    Ta = 373 # C (constant)
    E1_over_R = 4000 # Kelvin
    E2_over_R = 9000 # Kelvin
    Hr1a = -20000 # J/(mol of A reacted in reaction 1)
    Hr2a = -60000 # J/(mol of A reacted in reaction 2)
    
    #-----------------------------------------------------------------------------
    # defining initial conditions 
    ic = np.array([T,Fa,Fb,Fc])
    parameters = (T0,E1_over_R,E2_over_R,Ct0,Cpa,Cpb,Cpc,Ta,Hr1a,Hr2a,Ua)
    
    test = ODEs_CA3(ic,V1,parameters)
    print(test)
    output = odeint(ODEs_CA3,ic,Vspan, args = (parameters,))
    
    # Plotting
    plt.figure(1)
    
    plt.plot(Vspan, output[:, 0], label='Temperature', linestyle='--')
    plt.legend(loc='upper left')
    plt.title("Temperature profile")
    plt.xlabel("V(dm^3)")
    plt.ylabel("T(K)")
    plt.show()
    
    plt.figure(2)
    
    plt.plot(Vspan, output[:, 1], label='FA', linestyle=':')
    plt.plot(Vspan, output[:, 2], label='FB', linestyle='-.')
    plt.plot(Vspan, output[:, 3], label='FC', linestyle='-')
    plt.title("Molar flow rates FA, FB and FC")
    plt.legend(loc='upper right')
    plt.xlabel("V(dm^3)")
    plt.ylabel("F(mol/s)")
    
    plt.show()
    
    return 

main()




